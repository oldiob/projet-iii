/* From the kernel */

#ifndef barrier
#  define barrier() asm volatile ("":::"memory")
#endif

#ifdef CONFIG_SMP
#  define smp_read_barrier_depends() __sync_synchronize()
#else
#  define smp_read_barrier_depends() do { } while(0)
#endif

#define __READ_ONCE_SIZE						\
	({								\
		switch (size) {						\
		case 1: *(u8 *)res = *(volatile u8 *)p; break;		\
		case 2: *(u16 *)res = *(volatile u16 *)p; break;	\
		case 4: *(u32 *)res = *(volatile u32 *)p; break;	\
		case 8: *(u64 *)res = *(volatile u64 *)p; break;	\
		default:						\
			barrier();					\
			__builtin_memcpy((void *)res, (const void *)p, size); \
			barrier();					\
		}							\
	})

__always_inline
static void __read_once_size(const volatile void *p, void *res, int size)
{
	__READ_ONCE_SIZE;
}


#define __READ_ONCE(x, check)					\
	({							\
		union { typeof(x) __val; char __c[1]; } __u;	\
								\
		__read_once_size(&(x), __u.__c, sizeof(x));	\
		/* Enforce dependency ordering from x */ 	\
		smp_read_barrier_depends();			\
		__u.__val;					\
	})
#define READ_ONCE(x) __READ_ONCE(x, 1)

__always_inline
static void __write_once_size(volatile void *p, void *res, int size)
{
	switch (size) {
	case 1: *(volatile u8 *)p = *(u8 *)res; break;
	case 2: *(volatile u16 *)p = *(u16 *)res; break;
	case 4: *(volatile u32 *)p = *(u32 *)res; break;
	case 8: *(volatile u64 *)p = *(u64 *)res; break;
	default:
		barrier();
		__builtin_memcpy((void *)p, (const void *)res, size);
		barrier();
	}
}

#define WRITE_ONCE(x, val)					\
	({							\
		union { typeof(x) __val; char __c[1]; } __u =	\
			{ .__val = (__force typeof(x)) (val) }; \
		__write_once_size(&(x), __u.__c, sizeof(x));	\
		__u.__val;					\
	})
