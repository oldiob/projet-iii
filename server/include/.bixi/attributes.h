/*
 * Atributes:
 *
 * Taken from: Linux System Programming - Robert Love.  With some
 * little extra.
 */
#if __GNUC__ >= 3
#  define __alias(...)          __attribute__((alias(__VA_ARGS__)))
#  define __align_cache         __attribute__((__aligned__(CONFIG_D1CACHE_LINE_SIZE)))
#  define __always_inline       inline __attribute__((__always_inline__))
#  define __artificial          __attribute__((__artificial__))
#  define __copy(...)           __attribute__((copy(__VA_ARGS__)))
#  define __fallthrough         __attribute__((__fallthrough__))
#  define __flatten             __attribute__((__flatten__))
#  define __force
#  define __ctor                __attribute__((__constructor__))
#  define __dtor                __attribute__((__destructor__))
#  define __return_notnull      __attribute__((__returns_nonnull__))
#  define __notnull             __attribute__((__nonnull__))
#  define __non_null(...)       __attribute__((__nonnull__(__VA_ARGS__)))
#  define __noinline            __attribute__ ((__noinline__))
#  define __nonstring           __attribute__((__nonstring__))
#  define __pure                __attribute__ ((__pure__))
#  define __const               __attribute__ ((__const__))
#  define __format(f, n)        __attribute__ ((__format__(f, n, n+1)))
#  define __noreturn            __attribute__((__noreturn__))
#  define __malloc              __attribute__ ((__malloc__))
#  define __must_check          __attribute__ ((__warn_unused_result__))
#  define __section(s)          __attribute__((__section__(s)))
#  define __deprecated          __attribute__ ((__deprecated__))
#  define __used                __attribute__ ((used))
#  define __unused              __attribute__ ((__unused__))
#  define __packed              __attribute__ ((__packed__))
#  define __align(x)            __attribute__ ((__aligned__(x)))
#  define __align_max           __attribute__ ((__aligned__))
#  define likely(x)             __builtin_expect (!!(x), 1)
#  define unlikely(x)           __builtin_expect (!!(x), 0)
#  define __protected           __attribute__((visibility("protected")))
#else
#  define __alias(...)
#  define __align_cache
#  define __always_inline       inline
#  define __artificial
#  define __copy(...)
#  define __fallthrough         (void)0
#  define __flatten
#  define __force
#  define __ctor
#  define __dtor
#  define __return_notnull
#  define __notnull
#  define __non_null(...)
#  define __noinline
#  define __nonstring
#  define __pure
#  define __const
#  define __format(f, n)
#  define __noreturn
#  define __malloc
#  define __must_check
#  define __section(s)
#  define __deprecated
#  define __used
#  define __unused
#  define __packed
#  define __align(x)
#  define __align_max
#  define likely(x)
#  define unlikely(x)
#  define __protected
#endif	/* __GNUC__ */
