#include <assert.h>

#ifndef CONFIG_ASSERT
#  ifdef assert
#    undef assert
#  endif
#  define assert(...) (void)(__VA_ARGS__)
#endif

static inline bool assert_align_void(void *ptr, unsigned long align)
{
#ifndef CONFIG_ASSERT
	(void) ptr;
	(void) align;
#endif
	assert((unsigned long)ptr % align == 0);

	return true;
}
#define assert_align(PTR) assert_align_void(PTR, _Alignof(typeof(*PTR)))

static inline bool assert_pow2(unsigned long N)
{
#ifndef CONFIG_ASSERT
	(void)N;
#endif
	assert(N && !(N & (N-1)));

	return true;
}

#define static_assert_pow2(NUM)	static_assert(NUM && !(NUM & (NUM - 1)), STR(NUM) " must be a power of 2!")
