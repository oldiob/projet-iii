#ifndef NULL
#  define NULL ((void*)0)
#endif

#ifndef offsetof
#  define offsetof(type, member) __builtin_offsetof(type, member)
#endif

#ifndef streq
#  define streq(A, B) (0 == strcmp((A), (B)))
#endif

#define CAT_PRIMITIVE(X, Y) X ## Y
#define CAT(X, Y) CAT_PRIMITIVE(X, Y)

#define STR_PRIMITIVE(X) #X
#define STR(X) STR_PRIMITIVE(X)

#define MAKE_ID(PREFIX) CAT(PREFIX, __COUNTER__)

#if __x86_64__ || __aarch64__ || __powerpc64__ || __sparc__ || __mips__
#  define ARCH_ALIGNEMENT 8
#elif __i386__ || __arm__ || __m68k__ || __ILP32__
#  define ARCH_ALIGNEMENT 4
#endif

/* From the Kernel */
#define typecheck(x, y)           (!!(sizeof((typeof(x)*)1 == (typeof(y)*)1)))
#define field_sizeof(type, field) (sizeof((type*)0)->field)
#define field_size(x, field)      (sizeof((typeof(x)*)0)->field)
#define array_size(arr)           (sizeof(arr) / sizeof((arr)[0]))
#define paddingof(A, B)           ((alignof(A) - alignof(B)) % alignof(A))
#define alignof(x)                _Alignof(x)
#define range(a, b)               (a) ... (b)

#define container_of(ptr, type, member)				\
	({							\
		void *__$ptr = (void *)(ptr);			\
		((type *)(__$ptr - offsetof(type, member)));	\
	})
