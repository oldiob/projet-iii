#ifdef CONFIG_RELEASE
#  define if_debug(...) if (0)
#else
#  define if_debug(...) if (__VA_ARGS__)
#endif
